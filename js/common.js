$(document).ready(function() {


	$(window).scroll(function() {
    if ($(this).scrollTop() > 175){  
        $('.navbar').addClass("sticky");
      }
      else{
        $('.navbar').removeClass("sticky");
      }
    });
	    
    /***************** Wow.js ******************/
    
    new WOW().init();

	$(function() {
		$('.scroll-down img').click (function() {
		$('html, body').animate({scrollTop: $('.main-content').offset().top -110 }, 'slow');
			return false;
		});
	});

	$(function() {
		$('.scroll-up i').click (function() {
		$('html, body').animate({scrollTop: $('.body').offset().top -110 }, 'slow');
			return false;
		});
	});

	$(function() {
		$('.video-box img').click (function() {
			$('.video-box').toggleClass('open');
		});
	});

	$(function() {
		$('.search-dropdown').click (function() {
			$('.search-input').toggle();
		});
	});
	//SVG Fallback
	if(!Modernizr.svg) {
		$("img[src*='svg']").attr("src", function() {
			return $(this).attr("src").replace(".svg", ".png");
		});
	};

	//E-mail Ajax Send
	//Documentation & Example: https://github.com/agragregra/uniMail
	$("form").submit(function() { //Change
		var th = $(this);
		$.ajax({
			type: "POST",
			url: "mail.php", //Change
			data: th.serialize()
		}).done(function() {
			alert("Thank you!");
			setTimeout(function() {
				// Done Functions
				th.trigger("reset");
			}, 1000);
		});
		return false;
	});

	$("#owl-hero").owlCarousel({

		loop:true,
	    margin:10,
	    nav:false,
	    dots: true,
	    autoplay: true,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:1
	        },
	        1000:{
	            items:1
	        }
	    }

    });


    /***************** Owl Carousel Testimonials ******************/

    $("#owl-testi").owlCarousel({


        navigation: false, // Show next and prev buttons
        paginationSpeed: 400,
        singleItem: true,
        transitionStyle: "backSlide",
        autoPlay: true,
        responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:1
	        },
	        1000:{
	            items:1
	        }
	    }
	});
    $("#owl-testi-2").owlCarousel({


        navigation: false, // Show next and prev buttons
        paginationSpeed: 400,
        margin: 20,
        singleItem: true,
        transitionStyle: "backSlide",
        autoPlay: true,
        loop: true,
        responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:3
	        },
	        1000:{
	            items:5
	        }
	    }


	});
});